from __future__ import print_function, unicode_literals, absolute_import, division

# do not print warnings
import warnings
warnings.filterwarnings("ignore")

# mute errors, logs & info
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # or any {'0', '1', '2'}

# import miscellaneous modules
import tensorflow as tf
import keras

import shutil
import collections
import subprocess
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from glob import glob
import cv2
import os
import sys
import time
import datetime
import csv

# net 1
from csbdeep.utils import Path, normalize, to_color
from stardist import dist_to_coord, non_maximum_suppression, polygons_to_label
from stardist import random_label_cmap, draw_polygons, sample_points
from stardist import Config, StarDist

from scipy.ndimage import zoom
from skimage.measure import regionprops
from skimage.segmentation import find_boundaries
from tifffile import imread, imsave

# net 2
from keras.models import load_model
from keras import activations
from keras.preprocessing.image import ImageDataGenerator, img_to_array
# CAMs
from vis.utils import utils
from vis.utils.utils import apply_modifications
from vis.visualization import visualize_cam, visualize_saliency

# net 3
import keras_retinanet
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.visualization import draw_box, draw_caption
from keras_retinanet.utils.colors import label_color


# class and function defs
class Report:
	# net 1
	img_name = 'unkown'
	directory = 'unkown'
	predicted_class = -1
	predicted_labels_img = None
	single_nuclei = []
	single_nuclei_img_names = []
	single_nuclei_masks = []
	single_box_nuclei = []

	# net 2
	single_nuclei_classes = []
	cam_imgs = []
	
	# net 3
	single_nuclei_signals = []
	single_nuclei_signal_coordinates = []
	
	def __init__(self, img_name, directory):
		self.img_name = img_name
		self.directory = directory
		
		result_dir = self.result_dir()
		
		if os.path.exists(result_dir):
			shutil.rmtree(result_dir)
		elif not os.path.exists(directory + 'results'):
			print("No result dir found - exiting")
			sys.exit()
		os.mkdir(result_dir)
		os.mkdir(result_dir + 'single_nuclei')
		os.mkdir(result_dir + 'single_masks')
		os.mkdir(result_dir + 'single_box_nuclei')
		os.mkdir(result_dir + 'single_nuclei/access_nets')
		os.mkdir(result_dir + 'single_nuclei/cams')
		os.mkdir(result_dir + 'single_nuclei/signals')
		

	def img_path(self):
		return self.directory + 'data/' + self.img_name
	
	def single_nuclei_img_paths(self):
		return [self.result_dir() + 'single_nuclei/access_nets/' + name for 
				name in self.single_nuclei_img_names]
	
	def result_dir(self):
		return self.directory + 'results/' + self.img_name[:-4] + '/'

	def write_single_nucleus_files(self):
		for i in range(len(self.single_nuclei)):
			self.single_nuclei_img_names.append(str(i).zfill(4) + '.jpg')
			plt.imsave(self.result_dir() + 'single_nuclei/access_nets/' + self.single_nuclei_img_names[i], 
					   self.single_nuclei[i], format = 'jpg')
			plt.imsave(self.result_dir() + 'single_masks/' + self.single_nuclei_img_names[i], 
					   self.single_nuclei_masks[i], format = 'jpg')
			plt.imsave(self.result_dir() + 'single_box_nuclei/' + self.single_nuclei_img_names[i], 
					   self.single_box_nuclei[i], format = 'jpg')

	def write_labels(self):		
		# write predicted labels
		self.predicted_labels_img.savefig(self.result_dir() + 'all_predicted_nuclei.jpg', format = 'jpg')
		plt.close(self.predicted_labels_img)

	def write_nuc_cams(self):
		# net 2: write cams
		for name, cam_fig in zip(self.single_nuclei_img_names, self.cam_imgs):
			path = self.result_dir() + 'single_nuclei/cams/' + name
			cam_fig.savefig(path, format = 'jpg', bbox_inches='tight')
			plt.close(cam_fig)
	
	def write_sigs(self):		
		# net 3: write signal figures
		for name, signal_fig in zip(self.single_nuclei_img_names, self.single_nuclei_signals):
			path = self.result_dir() + 'single_nuclei/signals/' + name
			signal_fig.savefig(path, format = 'jpg', bbox_inches='tight')
			plt.close(signal_fig)


	def write(self):
		# write csv report file
		with open(self.result_dir() + 'report.csv','a+') as reportfile:
			wr = csv.writer(reportfile)
			
			# image path
			wr.writerow(['Evaluated FISH image file', '', '', '', '', '', '', self.img_path()])
			wr.writerow([])
			wr.writerow([])

			# summary - net 2
			wr.writerow(['SUMMARY OF NETWORKS'])
			net_2_gradings = collections.Counter(self.single_nuclei_classes)
			net_2_decision = image_wide_grading(net_2_gradings)
			wr.writerow(['Nucleus Classifier grading', net_2_decision])

			# summary - net 3
			single_nuclei_signal_gradings = map(net_3_grade, self.single_nuclei_signal_coordinates)
			net_3_grading_list = [grading for grading, _, _ in single_nuclei_signal_gradings]
			net_3_gradings = collections.Counter(net_3_grading_list)
			net_3_decision = image_wide_grading(net_3_gradings)
			wr.writerow(['Signal Detector grading', net_3_decision])
			wr.writerow([])
			wr.writerow([])

			
			# results net 1
			wr.writerow(['RESULTS NETWORK 1 - NUCLEUS DETECTOR'])
			wr.writerow(['detected nuclei overlayed on FISH image', '', '', '', '', '', '', self.result_dir() + 'all_predicted_nuclei.jpg'])
			wr.writerow([])
			for img_name in self.single_nuclei_img_names:
				path = self.result_dir() + 'single_nuclei/access_nets/' + img_name
				wr.writerow(['detected nucleus', '', '', '', '', '', '', path])
			wr.writerow([])
			wr.writerow([])
			
			# results net 2
			wr.writerow(['RESULTS NETWORK 2 - NUCLEUS CLASSIFIER'])
			wr.writerow(['Nucleus Classifier grading', net_2_decision])
			for grading, val in net_2_gradings.items():
				wr.writerow(['class counts', grading, val])
			wr.writerow([])
			
			for img_name, nuc_class in zip(self.single_nuclei_img_names, self.single_nuclei_classes):
				path = self.result_dir() + 'single_nuclei/access_nets/' + img_name
				wr.writerow(['detected nucleus classification', nuc_class, '', '', '', '', '', path])
			# write cam image	
				wr.writerow(['CAM', '', '', '', '', '', '', self.result_dir() + 'single_nuclei/cams/' + img_name])
			wr.writerow([])
			wr.writerow([])
			
			# results net 3
			wr.writerow(['RESULTS NETWORK 3 - SIGNAL DETECTOR'])
			wr.writerow(['Signal Detector grading', net_3_decision])
			for grading, val in net_3_gradings.items():
				wr.writerow(['class counts', grading, val])
			wr.writerow([])


			
			for img_name, nucleus in zip(self.single_nuclei_img_names, self.single_nuclei_signal_coordinates):
				path = self.result_dir() + 'single_nuclei/signals/' + img_name
				wr.writerow(['', 'signal class', 'confidence', 'x1', 'y1', 'x2', 'y2', ''])
				for signal in nucleus:
					wr.writerow(['detected signal', signal['signal_class'], str(signal['confidence'])] + list(signal['coord']) + [path])
				
				# signal counts
				for key, value in signal_counts(nucleus).items():
					wr.writerow(['signal counts', key, value])
					
				# grading
				grading, ratio, cep17_polysomy = net_3_grade(nucleus)
				with_polysomy = ' with CEP17 polysomy' if cep17_polysomy else ''
				wr.writerow(['nucleus classification', grading + with_polysomy + ' with ratio: ' + str(ratio)])
				wr.writerow([])


							
def signal_counts(nucleus):
	# signal counts
	signal_classes = [signal['signal_class'] for signal in nucleus]
	return collections.Counter(signal_classes)

def image_wide_grading(gradings):
	img_low_ratio = gradings['HER2 positive; low amplification'] / (gradings['HER2 positive; high amplification'] + 
																	gradings['HER2 positive; low amplification'] + 
																	gradings['HER2 negative; normal expression'])

	img_high_ratio = gradings['HER2 positive; high amplification'] / (gradings['HER2 positive; high amplification'] + 
																	gradings['HER2 positive; low amplification'] + 
																	gradings['HER2 negative; normal expression'])
	if img_low_ratio >= 0.2 and img_high_ratio < 0.4:
		decision = 'HER2 positive; low amplification'
	elif img_high_ratio >= 0.4 and img_low_ratio < 0.2:
		decision = 'HER2 positive; high amplification'
	elif img_low_ratio < 0.2 and img_high_ratio < 0.4:
		decision = 'HER2 negative; normal expression'
	return decision

def net_3_grade(nucleus):
	signals = signal_counts(nucleus)
	if signals['HER2-Cluster'] > 0:
		ratio = 10.0
	elif signals['HER2'] == 0:
		ratio = -1
	elif signals['CEP17'] == 0:
		ratio = -1
	else:
		ratio = signals['HER2'] / signals['CEP17']
	
	cep17_polysomy = signals['CEP17'] > 3
	
	if ratio == -1:
		return 'Artifact', ratio, cep17_polysomy
	elif ratio < 2.0:
		return 'HER2 negative; normal expression', ratio, cep17_polysomy
	elif ratio <= 5.0:
		return 'HER2 positive; low amplification', ratio, cep17_polysomy
	else:
		return 'HER2 positive; high amplification', ratio, cep17_polysomy

#network 1
def plot_labels(img, coord, prob, points, labels, lbl_cmap):
	fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(16,5))
	ax1, ax2, ax3 = ax.flatten()
	# input
	ax1.set_title('Input Image', fontsize=11)
	ax1.axis('off')
	ax1.imshow(img)
	# boundary labels
	ax2.set_title('Star-convex polygon fit', fontsize=11)
	ax2.axis('off')
	draw_polygons_own(ax2, coord, prob, points, show_dist=True)
	ax2.imshow(img)
	# filled labels
	ax3.set_title('Reconstructed labels from polygons', fontsize=11)
	ax3.axis('off')
	ax3.imshow(img)
	ax3.imshow(labels, cmap=lbl_cmap, alpha=0.3)
	fig.tight_layout()
	return fig

# code generate polygons only on first image
def _plot_polygon(ax, x,y,score,color):
	a,b = list(x),list(y)
	a += a[:1]
	b += b[:1]
	ax.plot(a,b,'--', alpha=1, linewidth=score, zorder=1, color=color)

def draw_polygons_own(ax, coord, score, poly_idx, cmap=None, show_dist=False):
	# poly_idx is a N x 2 array with row-col coordinate indices
	if cmap is None:
		cmap = random_label_cmap(len(poly_idx)+1)
	assert len(cmap.colors[1:]) >= len(poly_idx)
	for p,c in zip(poly_idx,cmap.colors[1:]):
		ax.plot(p[1],p[0],'.r', alpha=.5+0.5, markersize=4*score[p[0],p[1]], color=c)
		if show_dist:
			for x,y in zip(coord[p[0],p[1],1], coord[p[0],p[1],0]):
				ax.plot((p[1],x),(p[0],y),'-',color=c, linewidth=0.4)
		_plot_polygon(ax, coord[p[0],p[1],1], coord[p[0],p[1],0], 3*score[p[0],p[1]], color=c)


def get_all_box_regions(img, labels, mask_img = True):
	# save each polygon in BOUNDING BOX MANNER after non-maximum supression as separate image file -> https://github.com/mpicbg-csbd/stardist/issues/4 
	regions = regionprops(labels) 
	ss = tuple(r._slice for r in regions) 
	single_imgs = tuple(img[s] for s in ss) 
	single_masks = tuple(labels[s]==r.label for r,s in zip(regions, ss))
	return single_imgs, single_masks


def get_all_regions(img, labels, mask_img = True):
	# save each polygon after non-maximum supression as separate image file -> https://github.com/mpicbg-csbd/stardist/issues/4
	regions = regionprops(labels)
	ss = tuple(r._slice for r in regions)
	single_imgs = tuple(img[s] for s in ss)
	single_masks = tuple(labels[s]==r.label for r,s in zip(regions, ss))
	if mask_img:
		single_imgs = tuple(img*np.expand_dims(mask,-1) for img, mask in zip(single_imgs, single_masks))
	return single_imgs, single_masks


def export_nuc(regions, box_regions, reportobj):
	# nuclei
	for nucleus in regions[0]:
		zoomed = zoom(nucleus, (2, 2, 1), order=1)
		reportobj.single_nuclei.append(zoomed)
	# nuclei in bounding box fashion
	for nucleus in box_regions[0]:
		zoomed = zoom(nucleus, (2, 2, 1), order=1)
		reportobj.single_box_nuclei.append(zoomed)
	# masks for nuclei
	for mask in regions[1]:
		zoomed = zoom(mask, (2, 2), order=0, mode='nearest')
		reportobj.single_nuclei_masks.append(zoomed)
	return reportobj.single_nuclei, reportobj.single_box_nuclei, reportobj.single_nuclei_masks

# network 2
def img_gen(reportobj, batchsize):
	# keras data generator holding single nucleus images
	pred_dir = reportobj.result_dir() + 'single_nuclei/'
	pred_gen = ImageDataGenerator(rescale=1./255).flow_from_directory(
		pred_dir,
		batch_size=batchsize,
		class_mode='categorical',
		shuffle=False,
		seed=1337)
	# get number of single nuclei images
	num_test_imgs = len(pred_gen.classes)
	return pred_gen, num_test_imgs

def nuc_pred(generator, network, img_num, batchsize):
	#predict classes (artifact, bg, HER2...) of single nucleus images
	generator.reset()
	class_pred = network.predict_generator(generator, img_num // batchsize+1)
	pred = []
	for single_pred in class_pred:
		class_id = np.argmax(single_pred)
		if class_id == 0:
			class_name = 'Artifact'
		if class_id == 1:
			class_name = 'Background'
		if class_id == 2: 
			class_name = 'HER2 positive; high amplification'
		if class_id == 3:
			class_name = 'HER2 positive; low amplification'
		if class_id == 4:
			class_name = 'HER2 negative; normal expression'
		pred.append(class_name)
	return pred

def cam_gen(network, reportobj):
	# exchange activation function & generate CAM for each nucleus
	layer_idx = 20
	network.layers[layer_idx].activation = activations.linear
	linear_model = utils.apply_modifications(network)
	for elem in reportobj.single_nuclei_img_paths():
		img = utils.load_img(elem, target_size=(256, 256))
		grad = visualize_saliency(linear_model,
								  layer_idx,
								  filter_indices=None,
								  seed_input=img)
		reportobj.cam_imgs.append(plot_cam(elem, grad))

def plot_cam(path, grad):
	# plot CAM for each nucleus
	fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,10))
	ax1, ax2, ax3 = ax.flatten()
	# Input Image
	img = utils.load_img(path, target_size=(256, 256))
	ax1.set_title('Input Image', fontsize=11)
	ax1.axis('off')
	ax1.imshow(img)
	# Overlay Input Image and CAM'
	ax2.set_title('Overlay Input Image and CAM', fontsize=11)
	ax2.axis('off')
	ax2.imshow(img)
	ax2.imshow(grad, alpha=0.5)
	# CAM
	ax3.set_title('CAM', fontsize=11)
	ax3.axis('off')
	ax3.imshow(grad)
	fig.tight_layout()
	return fig


# network 3
def get_session():
	config = tf.ConfigProto()
	config.gpu_options.allow_growth = True
	return tf.Session(config=config)
# set the modified tf session as backend in keras
keras.backend.tensorflow_backend.set_session(get_session())

def sig_pred(nuc_imgs, network, iou_thresh, reportobj):
	# load and predict signal bounding boxes on each nucleus image
	# class name mappings
	labels_to_names = {0: 'CEP17', 1: 'HER2', 2: 'HER2-Cluster'}
	for img_path in nuc_imgs:
		# load image
		img = read_image_bgr(img_path)

		# copy to draw on img
		draw = img.copy()
		draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)
		img = preprocess_image(img)
		img, scale = resize_image(img)

		# predict objects
		boxes, scores, labels = network.predict_on_batch(np.expand_dims(img, axis=0))
		boxes /= scale

		# draw bounding boxes
		label_colors = {}
		for box, score, label in zip(boxes[0], scores[0], labels[0]):
			# sort scores
			if score < iou_thresh:
				break
			color = label_color(label)
			b = box.astype(int)
			draw_box(draw, b, color=color)
			label_colors[labels_to_names[label]] = color
			
		# get color and corresponding present labels for legend
			#https://stackoverflow.com/questions/3380726/converting-a-rgb-color-tuple-to-a-six-digit-code-in-python
			#https://matplotlib.org/users/legend_guide.html
		present_signals=[]
		for signal in label_colors.keys():
			if signal == 'CEP17':
				cep_label = mpatches.Patch(color='#%02x%02x%02x' %tuple(label_colors[signal]), label=signal)
				present_signals.append(cep_label)
			if signal == 'HER2':
				her2_label = mpatches.Patch(color='#%02x%02x%02x' %tuple(label_colors[signal]), label=signal)
				present_signals.append(her2_label)
			if signal == 'HER2-Cluster':
				cluster_label = mpatches.Patch(color='#%02x%02x%02x' %tuple(label_colors[signal]), label=signal)
				present_signals.append(cluster_label)
		
		# show detected objects on img
		fig = plt.figure(figsize=(5, 5))
		ax = fig.add_axes([0.1, 0.1, 0.55, 0.75]) # see https://stackoverflow.com/a/9651897
		ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), handles=present_signals)
		ax.axis('on')
		plt.imshow(draw)
		
		# save figures to list
		reportobj.single_nuclei_signals.append(fig)
		
		# save box label, coordinates and activation
		signals = []
		for box, score, label in zip(boxes[0], scores[0], labels[0]):
			if score < iou_thresh:
				continue
			signals.append({
				'coord':box.astype(int), 
				'confidence':score, 
				'signal_class':labels_to_names[label]
			})
		
		reportobj.single_nuclei_signal_coordinates.append(signals)