# DeepHER2 Pipeline

This repository contains the implementation of the DeepHER2 pipeline, as described in the MIDL2020 paper.

## Note
This pipeline is still work in progress and therefore does not strive to be a complete package, yet. For now, this preliminary state is used to represent the current progress.  

Furthermore, this pipeline was tested with [Keras 2.2.5](https://keras.io/) and [TensorFlow 1.12.0](https://www.tensorflow.org/). It additionally requires the following packages:
* [StarDist](https://github.com/mpicbg-csbd/stardist)
* [RetinaNet](https://github.com/fizyr/keras-retinanet)
* [keras-vis toolkit](https://github.com/raghakot/keras-vis)

We provided an [example jupyter notebook](https://gitlab.com/Avya/deepher2/blob/master/example/prediction.ipynb) to illustrate the current state and use case.